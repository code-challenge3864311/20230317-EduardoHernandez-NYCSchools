package com.puch0handsup.schoolscodechallenge.domain

import androidx.lifecycle.MutableLiveData
import com.puch0handsup.schoolscodechallenge.data.SchoolsRepository
import com.puch0handsup.schoolscodechallenge.data.SchoolsRepositoryImpl
import com.puch0handsup.schoolscodechallenge.domain.model.SatScore
import com.puch0handsup.schoolscodechallenge.utils.UIState
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class GetSatScoresUseCaseTest {

    private lateinit var testObject : GetSatScoresUseCase

    private val mockRepository = mockk<SchoolsRepository>(relaxed = true)
    private val testDispatcher = UnconfinedTestDispatcher()
    private val testScope = TestScope(testDispatcher)

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        testObject = GetSatScoresUseCase(mockRepository, testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        clearAllMocks()
    }

    @Test
    fun `get sat scores info when the repository calls the server to retrieve a list of sat scores returns SUCCESS state`() {
        coEvery { mockRepository.getSatScores() } returns mockk {
            every { isSuccessful } returns true
            every { body() } returns listOf(
                mockk(), mockk(), mockk(), mockk(), mockk()
            )
        }
        val state = MutableLiveData<UIState<List<SatScore>>>()

        val job = testScope.launch {
            testObject().collect {
                if (it is UIState.SUCCESS) {
                    assertEquals(3, it.response.size)
                }
            }
        }
        job.cancel()
    }

    @Test
    fun `get exception when the repository calls the server but retrieves a null response returns ERROR state`() {
        coEvery { mockRepository.getSatScores() } returns mockk {
            every { isSuccessful } returns true
            every { body() } returns null
        }

        val job = testScope.launch {
            testObject().collect {
                if (it is UIState.ERROR) {
                    assertEquals("Body came as null", it.error.localizedMessage)
                }
            }
        }
        job.cancel()
    }

    @Test
    fun `get exception when the repository fails to call the server to retrieve a list of sat scores returns ERROR state`() {
        coEvery { mockRepository.getSatScores() } returns mockk {
            every { isSuccessful } returns false
            every { errorBody() } returns mockk{
                Exception("There was a problem with the request")
            }
        }

        val job = testScope.launch {
            testObject().collect {
                if (it is UIState.ERROR) {
                    assertEquals("There was a problem with the request", it.error.localizedMessage)
                }
            }
        }
        job.cancel()
    }
}