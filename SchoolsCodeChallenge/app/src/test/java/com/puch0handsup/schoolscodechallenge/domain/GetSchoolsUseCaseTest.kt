package com.puch0handsup.schoolscodechallenge.domain

import com.puch0handsup.schoolscodechallenge.data.SchoolsRepository
import com.puch0handsup.schoolscodechallenge.utils.UIState
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class GetSchoolsUseCaseTest {

    private lateinit var testObject : GetSchoolsUseCase
    private var mockRepository = mockk<SchoolsRepository>(relaxed = true)

    private val testDispatcher = UnconfinedTestDispatcher()
    private val testScope = TestScope(testDispatcher)

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        testObject = GetSchoolsUseCase(mockRepository, testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        clearAllMocks()
    }

    @Test
    fun `get schools info when the repository calls the server to retrieve a list of schools returns SUCCESS state`() {
        // ASSIGNMENT
        coEvery { mockRepository.getSchools() } returns mockk {
            every { isSuccessful } returns true
            every { body() } returns listOf(mockk(), mockk(), mockk())
        }

        // ACTION
        val job = testScope.launch {
            testObject().collect {
                if (it is UIState.SUCCESS) {
                    assertEquals(3, it.response.size)
                }
            }
        }
        job.cancel()
    }

    @Test
    fun `get an exception when the repository calls the server to retrieve a null list of schools returns an ERROR state`() {
        // ASSIGNMENT
        coEvery { mockRepository.getSchools() } returns mockk {
            every { isSuccessful } returns true
            every { body() } returns null
        }

        // ACTION
        val job = testScope.launch {
            testObject().collect {
                if (it is UIState.ERROR) {
                    assertEquals("Body came as null", it.error.localizedMessage)
                }
            }
        }
        job.cancel()
    }

    @Test
    fun `get an exception when the repository calls the server to retrieve a failure response returns an ERROR state`() {
        // ASSIGNMENT
        coEvery { mockRepository.getSchools() } returns mockk {
            every { isSuccessful } returns false
            every { errorBody() } returns mockk {
                every { string() } returns "ERROR"
            }
        }

        // ACTION
        val job = testScope.launch {
            testObject().collect {
                if (it is UIState.ERROR) {
                    assertEquals("ERROR", it.error.localizedMessage)
                }
            }
        }
        job.cancel()
    }

    @Test
    fun `get an exception when the server throws an EXCEPTION returns an ERROR state`() {
        // ASSIGNMENT
        coEvery { mockRepository.getSchools() } throws Exception("ERROR")

        // ACTION
        val job = testScope.launch {
            testObject().collect {
                if (it is UIState.ERROR) {
                    assertEquals("ERROR", it.error)
                }
            }
        }
        job.cancel()
    }
}