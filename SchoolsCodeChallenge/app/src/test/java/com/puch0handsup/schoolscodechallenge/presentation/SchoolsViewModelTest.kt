package com.puch0handsup.schoolscodechallenge.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.puch0handsup.schoolscodechallenge.domain.GetSatScoresUseCase
import com.puch0handsup.schoolscodechallenge.domain.GetSchoolsUseCase
import com.puch0handsup.schoolscodechallenge.utils.UIState
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class SchoolsViewModelTest {

    @get:Rule val rule = InstantTaskExecutorRule()

    private lateinit var testObject : SchoolsViewModel
    private val mockSchoolsUseCase = mockk<GetSchoolsUseCase>(relaxed = true)
    private val mockScoresUseCase = mockk<GetSatScoresUseCase>(relaxed = true)

    private val mockDispatcher = UnconfinedTestDispatcher()

    @Before
    fun setUp() {
        Dispatchers.setMain(mockDispatcher)
        testObject = SchoolsViewModel(mockSchoolsUseCase, mockScoresUseCase, mockDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        clearAllMocks()
    }

    @Test
    fun `get schools when repository retrieves a list of schools returns SUCCESS state`() {
        every { mockSchoolsUseCase() } returns flowOf(
            UIState.SUCCESS(listOf(mockk(), mockk(), mockk(), mockk()))
        )
        testObject.schools.observeForever {
            if (it is UIState.SUCCESS) {
                assertEquals(4, (it.response.size))
            }
        }
        testObject.getSchools()
    }

    @Test
    fun `get error when repository retrieves a list of schools returns ERROR state`() {
        every { mockSchoolsUseCase() } returns flowOf(
            UIState.ERROR(Exception("ERROR"))
        )
        testObject.schools.observeForever {
            if (it is UIState.ERROR) {
                assertEquals("ERROR", (it.error.localizedMessage))
            }
        }
        testObject.getSchools()
    }

    @Test
    fun `get sat scores when repository retrieves a list of schools returns SUCCESS state`() {
        every { mockScoresUseCase() } returns flowOf(
            UIState.SUCCESS(listOf(mockk(), mockk(), mockk(), mockk()))
        )
        testObject.scores.observeForever {
            if (it is UIState.SUCCESS) {
                assertEquals(4, (it.response.size))
            }
        }
        testObject.getSchools()
    }

    @Test
    fun `get error response when repository retrieves a list of sat scores returns ERROR state`() {
        every { mockScoresUseCase() } returns flowOf(
            UIState.ERROR(Exception("ERROR"))
        )
        testObject.scores.observeForever {
            if (it is UIState.ERROR) {
                assertEquals("ERROR", (it.error.localizedMessage))
            }
        }
        testObject.getSchools()
    }
}