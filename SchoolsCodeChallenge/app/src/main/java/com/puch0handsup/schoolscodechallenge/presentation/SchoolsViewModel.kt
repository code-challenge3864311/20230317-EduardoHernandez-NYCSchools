package com.puch0handsup.schoolscodechallenge.presentation

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.puch0handsup.schoolscodechallenge.domain.GetSatScoresUseCase
import com.puch0handsup.schoolscodechallenge.domain.GetSchoolsUseCase
import com.puch0handsup.schoolscodechallenge.domain.model.SatScore
import com.puch0handsup.schoolscodechallenge.domain.model.School
import com.puch0handsup.schoolscodechallenge.utils.UIState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val TAG = "SchoolsViewModel"
@HiltViewModel
class SchoolsViewModel @Inject constructor(
    private val getSchoolsUseCase: GetSchoolsUseCase,
    private val getSatScoresUseCase: GetSatScoresUseCase,
    private val ioDispatcher: CoroutineDispatcher
): ViewModel() {

    private val _schools : MutableLiveData<UIState<List<School>>> = MutableLiveData(UIState.LOADING)
    val schools : LiveData<UIState<List<School>>> get() = _schools

    private val _scores : MutableLiveData<UIState<List<SatScore>>> = MutableLiveData(UIState.LOADING)
    val scores : LiveData<UIState<List<SatScore>>>  get() = _scores

    var selectedScore : SatScore? = null
    var schoolsFetched = false
    var scoresFetched = false

    fun getSchools() {
        viewModelScope.launch(ioDispatcher) {
            getSchoolsUseCase().collect {
                _schools.postValue(it)
                schoolsFetched = true
            }
        }
    }

    fun getSatScores() {
        viewModelScope.launch(ioDispatcher) {
            getSatScoresUseCase().collect {
                _scores.postValue(it)
                scoresFetched = true
            }
        }
    }

    fun getSchoolSatScore(dbn : String) : Boolean {
        Log.d(TAG, "getSchoolSatScore: $dbn")
        selectedScore = (scores.value as UIState.SUCCESS).response.find { it.dbn == dbn }
        selectedScore?.let {
            return true
        } ?: return false
    }
}