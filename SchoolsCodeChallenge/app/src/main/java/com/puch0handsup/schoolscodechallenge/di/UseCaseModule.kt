package com.puch0handsup.schoolscodechallenge.di

import com.puch0handsup.schoolscodechallenge.data.SchoolsRepository
import com.puch0handsup.schoolscodechallenge.data.SchoolsRepositoryImpl
import com.puch0handsup.schoolscodechallenge.data.ServiceApi
import com.puch0handsup.schoolscodechallenge.domain.GetSatScoresUseCase
import com.puch0handsup.schoolscodechallenge.domain.GetSchoolsUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

@Module
@InstallIn(SingletonComponent::class)
class UseCaseModule {

    @Provides
    fun providesRepository(
        serviceApi: ServiceApi
    ): SchoolsRepository = SchoolsRepositoryImpl(serviceApi)

    @Provides
    fun providesGetSchoolsUseCase(
        schoolsRepository: SchoolsRepository
    ): GetSchoolsUseCase = GetSchoolsUseCase(schoolsRepository)

    @Provides
    fun providesGetSatScoresUseCase(
        schoolsRepository: SchoolsRepository
    ): GetSatScoresUseCase = GetSatScoresUseCase(schoolsRepository)

    @Provides
    fun providesIoDispatcher() : CoroutineDispatcher = Dispatchers.IO
}