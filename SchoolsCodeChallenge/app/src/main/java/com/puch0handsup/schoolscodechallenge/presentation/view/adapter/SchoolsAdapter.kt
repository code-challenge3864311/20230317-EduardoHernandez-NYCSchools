package com.puch0handsup.schoolscodechallenge.presentation.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.puch0handsup.schoolscodechallenge.databinding.SchoolItemBinding
import com.puch0handsup.schoolscodechallenge.domain.model.School

class SchoolsAdapter(
    private val schools : MutableList<School> = mutableListOf(),
    private val onItemClick : (item: School) -> Unit
) : RecyclerView.Adapter<SchoolsViewHolder>() {

    fun updateSchools(newSchools : List<School>) {
        if (newSchools != schools) {
            schools.clear()
            schools.addAll(newSchools)
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolsViewHolder =
        SchoolsViewHolder(
            SchoolItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    override fun getItemCount(): Int = schools.size

    override fun onBindViewHolder(holder: SchoolsViewHolder, position: Int) =
        holder.bind(schools[position], onItemClick)
}

class SchoolsViewHolder(
    private val binding: SchoolItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(school: School, onItemClick: (School) -> Unit) {
        binding.schoolName.text = school.schoolName
        itemView.setOnClickListener { onItemClick(school) }
    }
}