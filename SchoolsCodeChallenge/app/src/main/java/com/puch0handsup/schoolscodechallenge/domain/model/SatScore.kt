package com.puch0handsup.schoolscodechallenge.domain.model

import com.puch0handsup.schoolscodechallenge.data.model.SatScoreItem

data class SatScore(
    val dbn: String,
    val numOfSatTestTakers: String,
    val satCriticalReadingAvgScore: String,
    val satMathAvgScore: String,
    val satWritingAvgScore: String,
    val schoolName: String
)

fun List<SatScoreItem>.mapToSatScore(): List<SatScore> =
    this.map {
        SatScore(
            dbn = it.dbn ?: "N/A",
            numOfSatTestTakers = it.numOfSatTestTakers ?: "Not available",
            satCriticalReadingAvgScore = it.satCriticalReadingAvgScore
                ?: "Not available",
            satMathAvgScore = it.satMathAvgScore ?: "Not available",
            satWritingAvgScore = it.satWritingAvgScore ?: "Not available",
            schoolName = it.schoolName ?: "Not available"
        )
    }

