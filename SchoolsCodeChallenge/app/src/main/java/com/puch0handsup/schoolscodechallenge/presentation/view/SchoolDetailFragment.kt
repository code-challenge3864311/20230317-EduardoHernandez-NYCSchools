package com.puch0handsup.schoolscodechallenge.presentation.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.puch0handsup.schoolscodechallenge.R
import com.puch0handsup.schoolscodechallenge.databinding.FragmentSchoolDetailBinding
import com.puch0handsup.schoolscodechallenge.domain.model.SatScore
import com.puch0handsup.schoolscodechallenge.utils.BaseFragment
import com.puch0handsup.schoolscodechallenge.utils.UIState

class SchoolDetailFragment : BaseFragment() {

    private val binding by lazy {
        FragmentSchoolDetailBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        viewModel.selectedScore?.let {
            updateSchool(it)
        } ?: run {
            makeToast("AN ERROR HAS OCCURRED")
        }
        return binding.root
    }

    private fun updateSchool(satScore : SatScore) {
        binding.schoolName.text = satScore.schoolName
        binding.mathScore.text = getString(R.string.math_avg_score, satScore.satMathAvgScore)
        binding.readingScore.text = getString(R.string.critical_reading_avg_score, satScore.satCriticalReadingAvgScore)
        binding.writingScore.text = getString(R.string.writing_avg_score, satScore.satWritingAvgScore)
    }
}