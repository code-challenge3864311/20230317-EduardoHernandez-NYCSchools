package com.puch0handsup.schoolscodechallenge.data

import com.puch0handsup.schoolscodechallenge.data.model.SatScoreItem
import com.puch0handsup.schoolscodechallenge.data.model.SchoolItem
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ServiceApi {

    @GET(SCHOOLS)
    suspend fun getSchools() : Response<List<SchoolItem>>

    @GET(SAT_SCORES)
    suspend fun getSatScores() : Response<List<SatScoreItem>>

    companion object {
        const val BASE_URL = "https://data.cityofnewyork.us/"
        private const val SCHOOLS = "resource/s3k6-pzi2.json"
        private const val SAT_SCORES = "resource/f9bf-2cp4.json"
    }
}