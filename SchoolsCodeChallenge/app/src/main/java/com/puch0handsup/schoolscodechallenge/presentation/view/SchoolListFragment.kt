package com.puch0handsup.schoolscodechallenge.presentation.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.puch0handsup.schoolscodechallenge.R
import com.puch0handsup.schoolscodechallenge.databinding.FragmentSchoolListBinding
import com.puch0handsup.schoolscodechallenge.domain.model.School
import com.puch0handsup.schoolscodechallenge.presentation.SchoolsViewModel
import com.puch0handsup.schoolscodechallenge.presentation.view.adapter.SchoolsAdapter
import com.puch0handsup.schoolscodechallenge.utils.BaseFragment
import com.puch0handsup.schoolscodechallenge.utils.UIState
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolListFragment : BaseFragment() {

    private val binding by lazy {
        FragmentSchoolListBinding.inflate(layoutInflater)
    }

    private val schoolsAdapter by lazy {
        SchoolsAdapter {
            if (viewModel.scoresFetched){
                if (viewModel.getSchoolSatScore(it.dbn))
                    findNavController().navigate(R.id.action_navigate_to_details_frag)
                else
                    makeToast("This school does not have a SAT score available")
            }
            else
                makeToast("Retrieving SAT scores. Please try again")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (viewModel.schoolsFetched) {
            schoolsAdapter.updateSchools((viewModel.schools.value as UIState.SUCCESS).response)
        } else {
            getSchools()
            viewModel.getSchools()
        }
        if (!viewModel.scoresFetched) {
            viewModel.getSatScores()
        }

        binding.rvSchools.apply {
            layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
            adapter = schoolsAdapter
        }

        return binding.root
    }

    private fun getSchools() {
        viewModel.schools.observe(viewLifecycleOwner) { state ->
            when(state) {
                is UIState.LOADING -> {
                    makeToast("Fetching results")
                }
                is UIState.SUCCESS -> {
                    schoolsAdapter.updateSchools(state.response)
                }
                is UIState.ERROR -> {
                    makeToast(state.error.localizedMessage ?: "Could not retrieve Schools")
                }
            }

        }
    }
}