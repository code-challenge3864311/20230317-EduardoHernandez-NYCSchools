package com.puch0handsup.schoolscodechallenge.di

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SchoolsApp : Application()