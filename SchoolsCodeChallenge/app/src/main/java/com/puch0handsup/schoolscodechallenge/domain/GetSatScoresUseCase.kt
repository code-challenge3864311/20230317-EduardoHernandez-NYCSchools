package com.puch0handsup.schoolscodechallenge.domain

import com.puch0handsup.schoolscodechallenge.data.SchoolsRepository
import com.puch0handsup.schoolscodechallenge.domain.model.SatScore
import com.puch0handsup.schoolscodechallenge.domain.model.mapToSatScore
import com.puch0handsup.schoolscodechallenge.utils.UIState
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class GetSatScoresUseCase @Inject constructor(
    private val schoolsRepository: SchoolsRepository,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) {
    operator fun invoke(): Flow<UIState<List<SatScore>>> = flow {
        emit(UIState.LOADING)
        try {
            val response = schoolsRepository.getSatScores()
            if (response.isSuccessful) {
                response.body()?.let {
                    val schools = it.mapToSatScore()
                    emit(UIState.SUCCESS(schools))
                } ?: throw Exception("Body came as null")
            } else throw Exception(response.errorBody()?.string())
        } catch (e: Exception) {
            emit(UIState.ERROR(e))
        }
    }.flowOn(ioDispatcher)
}