package com.puch0handsup.schoolscodechallenge.domain

import com.puch0handsup.schoolscodechallenge.data.SchoolsRepository
import com.puch0handsup.schoolscodechallenge.domain.model.School
import com.puch0handsup.schoolscodechallenge.domain.model.mapToSchool
import com.puch0handsup.schoolscodechallenge.utils.UIState
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class GetSchoolsUseCase @Inject constructor(
    private val schoolsRepository: SchoolsRepository,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) {
    operator fun invoke(): Flow<UIState<List<School>>> = flow {
        emit(UIState.LOADING)
        try {
            val response = schoolsRepository.getSchools()
            if (response.isSuccessful) {
                response.body()?.let {
                    val schools = it.mapToSchool()
                    emit(UIState.SUCCESS(schools))
                } ?: throw Exception("Body came as null")
            } else throw Exception(response.errorBody()?.string())
        } catch (e: Exception) {
            emit(UIState.ERROR(e))
        }
    }.flowOn(ioDispatcher)
}