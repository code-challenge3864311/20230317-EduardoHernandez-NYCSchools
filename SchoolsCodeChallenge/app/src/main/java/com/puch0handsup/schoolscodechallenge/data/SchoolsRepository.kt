package com.puch0handsup.schoolscodechallenge.data

import com.puch0handsup.schoolscodechallenge.data.model.SatScoreItem
import com.puch0handsup.schoolscodechallenge.data.model.SchoolItem
import retrofit2.Response
import javax.inject.Inject

interface SchoolsRepository {
    suspend fun getSchools() : Response<List<SchoolItem>>
    suspend fun getSatScores() : Response<List<SatScoreItem>>
}

class SchoolsRepositoryImpl @Inject constructor(
    private val serviceApi: ServiceApi
) : SchoolsRepository {
    override suspend fun getSchools(): Response<List<SchoolItem>> =
        serviceApi.getSchools()

    override suspend fun getSatScores(): Response<List<SatScoreItem>> =
        serviceApi.getSatScores()
}