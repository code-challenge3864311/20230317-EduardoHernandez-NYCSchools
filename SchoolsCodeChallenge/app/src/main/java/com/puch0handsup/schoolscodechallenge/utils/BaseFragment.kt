package com.puch0handsup.schoolscodechallenge.utils

import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.puch0handsup.schoolscodechallenge.presentation.SchoolsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
open class BaseFragment : Fragment() {

    protected val viewModel: SchoolsViewModel by activityViewModels()

    protected fun makeToast(message : String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

}