package com.puch0handsup.schoolscodechallenge.domain.model

import com.puch0handsup.schoolscodechallenge.data.model.SchoolItem

data class School (
    val dbn : String,
    val city: String,
    val latitude: String,
    val location: String,
    val longitude: String,
    val phoneNumber: String,
    val schoolEmail: String,
    val schoolName: String,
    val totalStudents: String,
    val website: String,
    val zip: String,
    val stateCode: String 
)

fun List<SchoolItem>.mapToSchool(): List<School> =
    this.map {
        School(
            dbn  = it.dbn ?: "N/A",
            city = it.city ?: "Not available",
            latitude = it.latitude ?: "Not available",
            location = it.location ?: "Not available",
            longitude = it.longitude ?: "Not available",
            phoneNumber = it.phoneNumber ?: "Not available",
            schoolEmail = it.schoolEmail ?: "Not available",
            schoolName = it.schoolName ?: "Not available",
            totalStudents = it.totalStudents ?: "Not available",
            website = it.website ?: "Not available",
            zip = it.zip ?: "Not available",
            stateCode = it.stateCode ?: "Not available"
        )
    }
